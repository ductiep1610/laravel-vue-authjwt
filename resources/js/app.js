require('./bootstrap');
import Vue from 'vue';
import App from './app/App.vue';
import { routes } from './app/routes';
import VueRouter from 'vue-router';
import axios from 'axios'
import VueAxios from 'vue-axios'
import { store } from './app/store.js';


const router = new VueRouter({
    routes: routes,
    mode: 'history',
});

Vue.use(VueAxios, axios);

const app = new Vue({
    el: '#app',
    router: router,
    store: store,
    render: app => app(App),
});
