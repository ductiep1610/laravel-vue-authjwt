import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './Home.vue';
import Login from './Login.vue';
import DashBoard from './DashBoard.vue';


Vue.use(VueRouter);


export const routes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/dashboard',
        component: DashBoard,
    },
];
