<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwtauth')->except('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['success' => false], Response::HTTP_UNAUTHORIZED);
        }
        return response()->json(['success' => true, 'token' => $token], Response::HTTP_OK);
    }

    public function checkToken()
    {
        return response()->json(['success' => true], Response::HTTP_OK);
    }

    public function logout()
    {
        $logout = auth()->logout();
        return response()->json(['success' => true], Response::HTTP_OK);
    }
}
